#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int main() {
    //liczba slow w pliku
    const int word_count = 15;
    char slowo[word_count][30];  // Zakładam, że plik zawiera 30 owoców

    // Otwarcie pliku do odczytu
    int temat=0;
    printf("Prosze wybrac temat slow:\n-owoce(1)\n-przedmioty szkolne(2)\n-zwiezeta(3)\n-kraje UE(4)\n-srodki transportu(5)\n");
    printf("Wprowdz odpowiednia litere: ");
    scanf("%d",&temat);
    FILE *wybrany_temat;
    switch (temat)
                {
                    case 1: wybrany_temat = fopen("./pliki_tekstowe/slowa_owoce.txt", "r"); break;
                    case 2: wybrany_temat = fopen("./pliki_tekstowe/slowa_przedmioty.txt", "r");  break;
                    case 3: wybrany_temat = fopen("./pliki_tekstowe/slowa_zwiezeta.txt", "r"); break;
                    case 4: wybrany_temat = fopen("./pliki_tekstowe/slowa_kraje_ue.txt", "r");  break; 
                    case 5: wybrany_temat = fopen("./pliki_tekstowe/slowa_srodki_transportu.txt", "r");  break; 
                    default :
                    printf("ERROR: Nieprawidlowy argument\n");
                    return 1; 
                    break;          
                }
    
    
    // Sprawdzenie, czy plik został poprawnie otwarty
    if (wybrany_temat == NULL) {
        printf("Nie można otworzyć pliku\n");
        return 1;  // Zwraca 1, aby wskazać, że program zakończył się błędem
    }

    // Wczytywanie danych z pliku do tablicy
    for (int i = 0; i < word_count; i++) {
        
        fscanf(wybrany_temat, "%20s", slowo[i]);
    }

    // Zamknięcie pliku
    fclose(wybrany_temat);
    //nickname
    char *nickname = (char *)malloc(20 * sizeof(char));
    printf("Podaj swoj nickname:\n");
    scanf("%s", nickname);
   
    srand(time(NULL));
    //zapisujemy w plik
    FILE *file = fopen("./ranking/gracze.txt", "a");

    if (file == NULL) {
        printf("Nie ma takiego plik\n");
        return 1;
    }
    fprintf(file, "%s\n", nickname);
    fclose(file);

    free(nickname);

    // losowe slowo
    int randomIndex = rand() % word_count;
    const char *selectedWord = slowo[randomIndex];


    //dlugosc slowa
    int wordLength = strlen(selectedWord);

    // tablica dla sliedzenia trafionych liter
    int *guessedLetters = malloc(wordLength * sizeof(int));
    for (int i = 0; i < wordLength; ++i) {
        guessedLetters[i] = 0; // na poczatku wszystkie litery nie trafione
    }

    // cykl gry
    int correctGuesses = 0;
    int right=2;
    int count_error = 0;
    while (correctGuesses < wordLength) {
       
        
    system("clear");
    // pole dla gry
    switch (count_error)
                {
                    case 0: system("./interface/etapy_gry/draw1"); break;
                    case 1: system("./interface/etapy_gry/draw2"); break;              
                    case 2: system("./interface/etapy_gry/draw3"); break;   
                    case 3: system("./interface/etapy_gry/draw4"); break; 
                    case 4: system("./interface/etapy_gry/draw5"); break;
                    case 5: system("./interface/etapy_gry/draw6"); break;              
                    case 6: system("./interface/etapy_gry/draw7"); break;   
                    case 7: system("./interface/etapy_gry/draw8"); break;                                
                }
   if(count_error==7)
   {printf("Koniec gry! Prosze sprobowac jeszcze raz!\n");
    printf("To bylo slowo: %s\n",selectedWord);
    break;}
        if (right==1) {
            printf("Brawo!\n");
        }
        
        if(right==0){
            printf("Spoboj jeszcze raz!\n");
        }
        right=0;
        
        // wyswietlenie slowa
        for (int i = 0; i < wordLength; ++i) {
            if (guessedLetters[i]) {
                printf("%c ", selectedWord[i]);
            } else {
                printf("_ ");
            }
        }
        printf("\n");

        // wpowadz litere
        printf("Prosze podac litere: ");
        char input_letter;
        scanf(" %c", &input_letter);

        // sprawdzanie czy jest taka litera
        int found = 0;
        for (int i = 0; i < wordLength; ++i) {
            if (!guessedLetters[i] && selectedWord[i] == input_letter) {
                guessedLetters[i] = 1;
                found = 1;
                correctGuesses++;
                right=1;
            }
        }
        if (right!=1)
            {   
                count_error++;
            }
        
    }

    // zwolniamy pamiec
    free(guessedLetters);
    
    
    if(count_error!=7)
    {printf("Gratulacje! To bylo slowo: |%s|\n", selectedWord);}

    return 0;
}